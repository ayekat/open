NAME
	open - open files/URIs with default applications as specified by the user

SYNOPSIS
	open [file|URI [...]]

DESCRIPTION
	Determines which application to use in order to open a file or a URI. This
	is achieved through detecting a file's MIME type or a URI's protocol type,
	and launching an application according to the user's configuration.

	Passing no argument is equal to 'open .'

FILES
	${XDG_CONFIG_HOME}/open/openrc holds the configuration and contains several
	types of mappings. If the file does not exist upon invoking open, it shall
	be created with an empty configuration.

	If ${XDG_CONFIG_HOME} is not set, open will default to ~/.config.

	The mappings may be of following types:

	<MIME_TYPE> : <SHELL_COMMAND>
	This is probably the main usecase: it maps a MIME type to a specified
	command line. Note that the command will be passed to /bin/sh, which allows
	the definition of multiple commands, as in regular shell scripting.

	placeholder : <PLACEHOLDER_SEQUENCE>
	Usually the file/URI argument is simply appended to the command line.
	However it may be useful for certain commands to insert the file/URI
	argument somewhere inside the command line.
	If the command line contains a sequence as defined by 'placeholder', the
	file/URI argument is not simply appended, but substitutes the placeholder
	sequence. There may be multiple placeholders per line.

	<EXTENSION> : <MIME_TYPE>
	In case a file type is not recognised (yields application/octet-stream), the
	open utility shall attempt to make a second guess by looking at the file's
	extension. The user's configuration shall be searched for a mapping from an
	extension to a MIME type, which is then used to do the "normal" mapping.
	<EXTENSION> MUST contain the leading .

	<URI_PROTOCOL> : <MIME_TYPE>
	In case the given argument is not a file, but a URI (e.g. http://, ftp://,
	ssh://), the protocol will be determined, and the user's configuration shall
	be searched for a mapping from a protocol to a MIME type, which is then used
	to do the "normal" mapping.
	<URI_PROTOCOL> must NOT contain the trailing :

	# <COMMENT>
	Technically speaking, everything that is unlikely to be mapped is ignored,
	so it is not mandatory to put # in front of comments. However it improves
	human readability, and also greatly reduces the risk of accidentally being
	used for a mapping.

EXAMPLE
	An example openrc file may be found here:
		http://github.com/ayekat/dotfiles/tree/master/.local/etc/open/openrc

NOTE
	You may most likely be interested in using a file manager to do the
	MIME-to-default-application mapping, as it uses Desktop files as understood
	by most graphical environments and defined by freedesktop.org; the open
	utility is not very portable.

	open was written to provide a default-application handling without the need
	for having a full graphical file manager or keeping any compatibility with
	standards, and to provide simple, hand-written configuration.

EXIT STATUS
	If a file/URI can be successfully mapped to a command line, open shall
	return 0; in all other cases, it shall return a non-zero value.

DEPENDENCIES
	The open utility makes use of very basic utilities that should be available
	on all Unix-like systems (grep, awk, file, sh, cut, sed, echo).

BUGS
	There certainly are plenty of them. Feel free to submit bug reports at
	http://github.com/ayekat/utils/. I'm an hobby developer, I won't bite ;-)

AUTHOR
	Written by Tinu Weber (http://ayekat.ch/).

COPYRIGHT
	Copyright (c) 2014 Tinu Weber. Licensed under the GNU General Public License
	version 3 (GPLv3, http://gnu.org/licenses/gpl.html).

SEE ALSO
	xdg-open(1), thunar(1), file(1)
